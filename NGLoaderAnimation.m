//
//  NGLoaderAnimation.m
//  NGLoader
//
//  Created by Akanksha Sharma on 14/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import "NGLoaderAnimation.h"

@implementation NGLoaderAnimation

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/**
*  Init the image view animation loaders
*
*  @param topView           view on which loader is to be placed
*  @param frameRatio        ratio of loader w.r.t. the view
*  @param animationDuration animation duration
*/

-(void) initAnimationViewOnView : (UIView *) topView  withFrameRatio : (float) frameRatio WithDuration : (NSTimeInterval) animationDuration {

CGFloat topViewWidth = topView.frame.size.width;
CGFloat topViewHeight = topView.frame.size.height;
CGFloat animationViewSideLength  = MIN(topViewWidth, topViewHeight) * frameRatio;
[topView addSubview:self];
    CGRect myFrame = CGRectMake(((topViewWidth/2) - (animationViewSideLength/2)), ((topViewHeight/2) - (animationViewSideLength/2)), animationViewSideLength, animationViewSideLength);
    [self setFrame:myFrame];
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 1; i < 30; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"Loader_4_000%i.png",i]]];
    }
    self.animationImages = images;
    self.animationDuration = animationDuration;
    
}

@end

//
//  NGLoaderAnimation.h
//  NGLoader
//
//  Created by Akanksha Sharma on 14/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGLoaderAnimation : UIImageView

-(void) initAnimationViewOnView : (UIView *) view  withFrameRatio : (float) frameRatio WithDuration : (NSTimeInterval) animationDuration;



@end
